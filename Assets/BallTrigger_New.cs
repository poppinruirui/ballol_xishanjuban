﻿/*
 * 这个文件中的代码可以好好重构一次，太乱了。性能很低下
 * 
 * */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrigger_New : MonoBehaviour {

    static Vector2 vec2Temp = new Vector2();


    public Ball _ball;
	
	public Thorn _thorn;
	public eTriggerType _type;

	public enum eTriggerType
	{
		ball,
		bean,
		thorn,
		soft_obstacle,
		hard_obstacle,
        spore,
	};

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	// 接触
	void OnTriggerEnter2D(Collider2D other)
	{
        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
		{
			return;
		}

        if (this._type == eTriggerType.spore  && other.transform.gameObject.tag == "thorn")
        {
            CMonster thorn = other.transform.gameObject.GetComponent<CMonster>();
            if (thorn)
            {
                if (thorn.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.thorn)
                {
                    CMonster spore = this.gameObject.GetComponent<CMonster>();
                    if (spore && spore.IsEjecting() )
                    {
                        spore.EndEject();
                        Player player = spore.GetPlayer();
                        if (player && player.IsMainPlayer())
                        {
                            vec2Temp = thorn.GetPos() - spore.GetPos();
                            vec2Temp.Normalize();
                            player.PushThorn( thorn.GetGuid(), thorn.GetPos(), vec2Temp, spore.GetDir());
                        }
                    }
                }
            }


        }

        if (this._type == eTriggerType.ball && other.transform.gameObject.tag == "caidan")
        {
            other.transform.gameObject.SetActive(false);
            Main.s_Instance.m_goHongBao.SetActive( true );
        }

        if ( this.gameObject.tag == "thorn" )
        {
            CMonster jueshengqiu = this.gameObject.GetComponent<CMonster>();
            if (jueshengqiu)
            {
                if (jueshengqiu.m_bIsJueSheng)
                {
                    CMonster thorn = other.transform.gameObject.GetComponent<CMonster>();
                    if (thorn)
                    {
                        GameObject.Destroy( thorn.gameObject );
                    }
                }
            }
        }

        /*
		if (other.transform.gameObject.tag == "thorn") {
			CMonster thorn = other.transform.gameObject.GetComponent<CMonster> ();
			if (thorn) {
                if (thorn.GetMonsterBuildingType() != CMonsterEditor.eMonsterBuildingType.thorn)
                {
                    this._ball.EatThorn(thorn);
                }
			}
		} else*/ if (other.transform.gameObject.tag == "bean") {
			CMonster bean = other.transform.gameObject.GetComponent<CMonster> ();
			if (bean) {
				this._ball.EatBean (bean);
			}
		} else if (other.transform.gameObject.tag == "Grass") {
			CGrass grass = other.transform.gameObject.GetComponent<CGrass> ();
			this._ball.EnterGrass (grass);
		} 

    }

    void OnTriggerStay2D(Collider2D other)
    {
      if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
        {
            return;
        }

        if (this._type != eTriggerType.ball)
        {
            return;
        }
        if (this._ball.IsDead())
        {
            return;
        }

        if (other.transform.gameObject.tag == "thorn")
        {
			CMonster thorn = other.transform.gameObject.GetComponent<CMonster>();
            if (thorn)
            {
                    this._ball.ProcessPk_Monster(thorn);
            }
        }
        else if (other.transform.gameObject.tag == "Mouth")
        {
            Ball ballOpponent = other.transform.parent.gameObject.GetComponent<Ball>();
            if (ballOpponent)
            {
                //this._ball._Player.ProcessCollideEnterGroup ( this._ball, ballOpponent ); 
                this._ball.ProcessPk(ballOpponent);
            }
        }
        else if (other.transform.gameObject.tag == "BeanSpray_Main")
        {
            Spray spray = other.transform.parent.gameObject.GetComponent<Spray>();
            if (spray)
            {
                spray.GenerateMeatBean(this._ball);
            }
            //this._ball._Player.DestroyBall ( this._ball.GetIndex() );
        }
        else if (other.transform.gameObject.tag == "ClassCircle")
        {
            this._ball.StayOnClassCircle(other.transform.gameObject.name);
		}else if (other.transform.gameObject.tag == "Spray") {
			CSpray spray = other.transform.gameObject.GetComponent<CSpray> ();
			if (spray) {
				spray.ProcessOnStay ( this._ball );
			}
		}
        else if ( other.gameObject.tag == "Dust" )
        {
            this._ball.SetDustCollierEnalbed( true );

            if (this._ball._Player.IsMainPlayer())
            {
                CDust dust = other.gameObject.GetComponent<CDust>();
                if (dust)
                {
                    this._ball._Player.AddDustToSyncPool(dust);
                }
            } 

            if (this._ball.IsEjecting())
            {
                this._ball.SlowDownEject();
            }
        }
        
    }

	void OnTriggerExit2D(Collider2D other)
	{
        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
		{
			return;
		}

		if (this._type != eTriggerType.ball)
		{
			return;
		}

	    if (other.transform.gameObject.tag == "Grass") {
			CGrass grass = other.transform.gameObject.GetComponent<CGrass> ();
			this._ball.LeaveGrass ();
		}
    }
}
