﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitBallTarget : MonoBehaviour {

	Ball m_ball = null;
	public SpriteRenderer m_sr = null;
	float m_fTotalTime = 0f;
	bool m_bCouting = false;
	static Vector3 vecTempPos = new Vector3 ();
	static Vector3 vecTempScale = new Vector3 ();

	// Use this for initialization
	void Awake () {
		
		m_sr = this.gameObject.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if (m_ball != null) {
			vecTempPos = m_vecPos;
			vecTempPos.x /= m_ball.GetSize();
			vecTempPos.y /= m_ball.GetSize ();
			float fSize = m_fSize / m_ball.GetSize ();
			vecTempScale.x = fSize;
			vecTempScale.y = fSize;
			vecTempScale.z = 1f;
			this.transform.localPosition = vecTempPos;
			this.transform.localScale = vecTempScale;
		}
		*/
	}

	public void SetVisible (bool val)
	{
		this.gameObject.SetActive ( val );
	}

	public void SetBall( Ball ball )
	{
		m_ball = ball;
	}

	public float GetBoundsSize()
	{
		return m_sr.bounds.size.x;
	}

	Vector3 m_vecPos = new Vector3();
	public void SetPos( Vector3 pos )
	{
		this.transform.position = pos;
	}

	float m_fSize = 0f;
	public void SetSize( float size )
	{
		m_fSize = size;
		vecTempScale.x = m_fSize;
		vecTempScale.y = m_fSize;
		vecTempScale.z = 1f;
		this.transform.localScale = vecTempScale;
	}


    static byte[] m_Blob8 = new byte[8];
    static byte[] m_Blob16 = new byte[16];
    static byte[] m_Blob24 = new byte[24];
    static byte[] m_Blob32 = new byte[32];
    static byte[] m_Blob64 = new byte[64];
    static byte[] m_Blob128 = new byte[128];
    static byte[] m_Blob256 = new byte[256];
    static Dictionary<int, byte[]> m_dicBlobs = new Dictionary<int, byte[]>();
    public static void InitBlobs()
    {

        m_dicBlobs[8] = m_Blob8;
        m_dicBlobs[16] = m_Blob16;
        m_dicBlobs[24] = m_Blob24;
        m_dicBlobs[32] = m_Blob32;
        m_dicBlobs[64] = m_Blob64;
        m_dicBlobs[128] = m_Blob128;
        m_dicBlobs[256] = m_Blob256;


    }

    public static byte[] ReuseBlob(int key)
    {
        byte[] blob = null;
        if (!m_dicBlobs.TryGetValue(key, out blob))
        {
            Debug.LogError("有Bug,ReuseBlob失败");
        }
        return blob;
    }
}
