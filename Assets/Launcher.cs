﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//namespace Com.KeMengWorld.ElectricMusicBall
//{
    public class Launcher : Photon.PunBehaviour
    {
        #region Public Variables
		public byte MaxPlayersPerRoom = 20;

		[Tooltip("The Ui Panel to let the user enter name, connect and play")]
		public GameObject controlPanel;
		[Tooltip("The UI Label to inform the user that the connection is in progress")]
		public GameObject progressLabel;

	public InputField _inputRoomName;


        #endregion
 
 
        #region Private Variables
 
 
        /// <summary>
        /// This client's version number. Users are separated from each other by gameversion (which allows you to make breaking changes).
        /// </summary>
        string _gameVersion = "1";
		
		/// <summary>
		/// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
		/// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
		/// Typically this is used for the OnConnectedToMaster() callback.
		/// </summary>
		bool isConnecting;
 
        #endregion
 
 
        #region MonoBehaviour CallBacks
 
 
        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        void Awake()
        {
            // #Critical
            // we don't join the lobby. There is no need to join a lobby to get the list of rooms.
		PhotonNetwork.autoJoinLobby = false;
 
            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.automaticallySyncScene = true;

		_inputRoomName.text = "默认房间";
        }
 
		/*
		static ulong rand_seed;  
		void  myrand ()  
		{  
			rand_seed = (rand_seed * 16807L) % ((1 << 31) - 1);  
			Debug.Log (rand_seed);  
		}  
		*/
 
        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        void Start()
        {
            //Connect();
			progressLabel.SetActive(false);
			controlPanel.SetActive(true);
		
			m_eGameMode = eGameMode.yaqiu;

		}

	void Update()
	{
		Worinima ();

		RoomInfo[] artRooms =  PhotonNetwork.GetRoomList();
		for (int i = 0; i < artRooms.Length; i++) {
			RoomInfo room = artRooms [i];
			Debug.Log ( room.Name );
		}
	}
 
 
        #endregion
 
 
        #region Public Methods
 
 
        /// <summary>
        /// Start the connection process. 
        /// - If already connected, we attempt joining a random room
        /// - if not yet connected, Connect this application instance to Photon Cloud Network
        /// </summary>
        public void Connect()
        {
        m_eSceneMode = eSceneMode.Game; // “运行游戏”模式

        // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
        isConnecting = true;
			
			progressLabel.SetActive(true);
			controlPanel.SetActive(false);
 
            // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
            if (PhotonNetwork.connected)
            {
                // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnPhotonRandomJoinFailed() and we'll create one.
			PhotonNetwork.JoinOrCreateRoom( _inputRoomName.text, new RoomOptions() { MaxPlayers = 20/*MaxPlayersPerRoom*/ }, null);
           }
        else
			{
                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.ConnectUsingSettings(_gameVersion);
            }
        }
 
		 void OnConnectedToMaster()
		{
		 
			Debug.Log("连上了Master Server.  Mater Server是用于分配游戏房间的服务器");
			
			// #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnPhotonRandomJoinFailed()  
			// we don't want to do anything if we are not attempting to join a room. 
			// this case where isConnecting is false is typically when you lost or quit the game, when this level is loaded, OnConnectedToMaster will be called, in that case
			// we don't want to do anything.
			if (isConnecting)
			{
				//PhotonNetwork.JoinRandomRoom(); 
			PhotonNetwork.JoinOrCreateRoom ( _inputRoomName.text, new RoomOptions() { MaxPlayers = 20/*MaxPlayersPerRoom*/ }, null );
			}
		 
		}
		
		public override void OnDisconnectedFromPhoton()
		{
		 
			progressLabel.SetActive(false);
			controlPanel.SetActive(true);
			Debug.LogWarning("OnDisconnectedFromPhoton() was called by PUN");        
			Connect ();
		}
 
 		public override void OnPhotonRandomJoinFailed (object[] codeAndMsg)
		{
			Debug.Log("DemoAnimator/Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");
		 
			// #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
		PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 20/*MaxPlayersPerRoom*/ }, null);
		}
		 
		public override void OnJoinedRoom()   
		{
			Debug.Log("DemoAnimator/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
			// #Critical: We only load if we are the first player, else we rely on  PhotonNetwork.automaticallySyncScene to sync our instance scene.
			if (PhotonNetwork.room.PlayerCount == 1)
			{
			Debug.Log("登入房间成功： " + PhotonNetwork.room.Name);


                // #Critical
                // Load the Room Level. 
            
                PhotonNetwork.LoadLevel("Scene");
			}
        m_eSceneMode = eSceneMode.Game; // “运行游戏”模式
    }

        // 进入地编
        public void EnterMapEditor()
        {
            SceneManager.LoadScene("Scene"); // 
          m_eSceneMode = eSceneMode.MapEditor; // “地图编辑器”模式
    }

	public  Toggle _toggleKuoZhang;
	public  Toggle _toggleYaQiu;
	public  Toggle _toggleXiQiu;

	public enum eGameMode
	{
		kuangzhang, // 扩张模式
		yaqiu,      // 压球模式
		xiqiu,      // 吸球模式
	};

	public static eGameMode m_eGameMode = eGameMode.yaqiu;

	public void OnToggleValueChanged_KuoZhang()
	{  
		if (_toggleKuoZhang.isOn) {
			_toggleYaQiu.isOn = false;
			m_eGameMode = eGameMode.kuangzhang;
			_toggleXiQiu.isOn = false;
		} 
	}

	public void OnToggleValueChanged_YaQiu()
	{
		if (_toggleYaQiu.isOn) {
			_toggleKuoZhang.isOn = false;
			m_eGameMode = eGameMode.yaqiu;
			_toggleXiQiu.isOn = false;
		}
	}

	public void OnToggleValueChanged_XiQiu()
	{
		if (_toggleXiQiu.isOn) {
			_toggleKuoZhang.isOn = false;
			m_eGameMode = eGameMode.xiqiu;
			_toggleYaQiu.isOn = false;

		} 
	}

	void Worinima()
	{
		if (_toggleKuoZhang.isOn == false && _toggleYaQiu.isOn == false && _toggleXiQiu.isOn == false) {
			if (m_eGameMode == eGameMode.kuangzhang) {
				_toggleKuoZhang.isOn = true;
			} else if (m_eGameMode == eGameMode.yaqiu) {
				_toggleYaQiu.isOn = true;
			} else if (m_eGameMode == eGameMode.xiqiu) {
				_toggleXiQiu.isOn = true;
			}
		}

		if (_toggleKuoZhang.isOn == true) {
			m_eGameMode = eGameMode.kuangzhang;
		}
		if (_toggleYaQiu.isOn == true) {
			m_eGameMode = eGameMode.yaqiu;
		}

		if (_toggleXiQiu.isOn == true) {
			m_eGameMode = eGameMode.xiqiu;
		}

	}

	public void EnterLobby()
	{
		SceneManager.LoadScene("Lobby"); 
	}

	      public enum eSceneMode
        {
            Game,
            MapEditor,
        };

        public static eSceneMode m_eSceneMode = eSceneMode.MapEditor;
    #endregion


    }
//}