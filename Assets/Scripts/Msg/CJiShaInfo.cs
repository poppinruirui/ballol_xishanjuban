﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CJiShaInfo : MonoBehaviour {

    public static CJiShaInfo s_Instance;

    public float m_fShowTime = 5f;
    float m_fShowTimeCount = 0f;

    /// <summary>
    /// / UI
    /// </summary>
    public GameObject _panelJiSha;
    public Text       _txtKillerName;
    public Text       _txtDeadMeatName;
    public Image      _imgKillerAvatar;
    public Image      _imgDeadMeatAvatar;

    public GameObject _panelJiSha_PC;
    public Text       _txtKillerName_PC;
    public Text       _txtDeadMeatName_PC;
    public Image      _imgKillerAvatar_PC;
    public Image      _imgDeadMeatAvatar_PC;

    public GameObject _panelJiSha_MOBILE;
    public Text       _txtKillerName_MOBILE;
    public Text       _txtDeadMeatName_MOBILE;
    public Image      _imgKillerAvatar_MOBILE;
    public Image      _imgDeadMeatAvatar_MOBILE;

    /// <summary>
    /// / end UI
    /// </summary>

    bool m_bShow = false;

    // Use this for initialization
    void Start () {
		
	}

    private void Awake()
    {
        s_Instance = this;
    }

    // Update is called once per frame
    void Update () {
        Counting();

        InitUI();
    }

    bool m_bUIInited = false;
    void InitUI()
    {
        if ( Main.s_Instance == null )
        {
            return;
        }

        if (m_bUIInited)
        {
            return;
        }

        if ( Main.s_Instance.GetPlatformType() == Main.ePlatformType.pc )
        {
            _panelJiSha       =_panelJiSha_PC;
            _txtKillerName=       _txtKillerName_PC;
            _txtDeadMeatName=     _txtDeadMeatName_PC;
            _imgKillerAvatar=     _imgKillerAvatar_PC;
            _imgDeadMeatAvatar=   _imgDeadMeatAvatar_PC;
        }
        else if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
        {
            _panelJiSha = _panelJiSha_MOBILE;
            _txtKillerName=    _txtKillerName_MOBILE;
            _txtDeadMeatName=  _txtDeadMeatName_MOBILE;
            _imgKillerAvatar=  _imgKillerAvatar_MOBILE;
            _imgDeadMeatAvatar=_imgDeadMeatAvatar_MOBILE;
        }

        m_bUIInited = true;

    }

    public void Show(string szKillerName, string szDeadMeatName, Sprite sprKillerAvatar, Sprite sprDeadMeatAvatar)
    {
        _panelJiSha.SetActive( true );
        _txtKillerName.text = szKillerName;
        _txtDeadMeatName.text = szDeadMeatName;
        _imgKillerAvatar.sprite = sprKillerAvatar;
        _imgDeadMeatAvatar.sprite = sprDeadMeatAvatar;
        m_bShow = true;
        m_fShowTimeCount = 0;
    }

    public void Hide()
    {
        m_bShow = false;
        _panelJiSha.gameObject.SetActive(false);
    }

    void Counting()
    {
        if ( !m_bShow)
        {
            return;
        }
        m_fShowTimeCount += Time.deltaTime;
        if (m_fShowTimeCount >= m_fShowTime)
        {
            Hide();
        }
    }
}
