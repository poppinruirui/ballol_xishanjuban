﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
public class UIManager : MonoBehaviour {

    public const string TAG_UI_PC = "UI_GAME";
    public const string TAG_UI_MOBILE = "UI_GAME_MOBILE";

    public const float c_StandardWidth = 1920f;
    public const float c_StandardHeight = 1080f;
    static Vector3 vecTempPos = new Vector3();

    GameObject[] m_aryUIPanelGame_PC ;
    GameObject[] m_aryUIPanelGame_MOBIE;

    public static UIManager s_Instance;

    public CanvasScaler _canvasScaler;

    private void Awake()
    {
        s_Instance = this;
    }

    void RefreshUIPos()
    {
     


    }

    // Use this for initialization
    void Start () {

        m_aryUIPanelGame_PC = GameObject.FindGameObjectsWithTag(TAG_UI_PC);
        m_aryUIPanelGame_MOBIE = GameObject.FindGameObjectsWithTag(TAG_UI_MOBILE);

        if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor )
        {
            _canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
            _canvasScaler.scaleFactor = 1.6f;
        }

    }
	
    public void SetUiGameVisible( bool val )
    {
        if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.pc)
        {
            for (int i = 0; i < m_aryUIPanelGame_PC.Length; i++)
            {
                m_aryUIPanelGame_PC[i].SetActive(val);
            }
        }
        else if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
        {
            for (int i = 0; i < m_aryUIPanelGame_MOBIE.Length; i++)
            {
                m_aryUIPanelGame_MOBIE[i].SetActive(val);
            }
        }
    }

	// Update is called once per frame
	void Update () {
        RefreshUIPos();

    }


	public static  void UpdateDropdownView( Dropdown dropdownItem,  List<string> showNames)
	{
		if (dropdownItem == null) {
			return;
		}

		dropdownItem.options.Clear();
		Dropdown.OptionData tempData;
		for (int i = 0; i < showNames.Count; i++)
		{
			tempData = new Dropdown.OptionData();
			tempData.text = showNames[i];
			dropdownItem.options.Add(tempData);
		}
		if (showNames.Count > 0) {
			dropdownItem.captionText.text = showNames [0];
		}
	}

    // 判断当前是否点击在了UI上
    public static bool IsPointerOverUI()
    {

        PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
        eventData.pressPosition = Input.mousePosition;
        eventData.position = Input.mousePosition;

        List<RaycastResult> list = new List<RaycastResult>();
        UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

        return list.Count > 0;

    }
}
