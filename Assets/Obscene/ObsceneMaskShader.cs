﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsceneMaskShader : MonoBehaviour {

	public GameObject _circle1;
	public GameObject _circle2;
	public GameObject _imaginarycircle;

	public SpriteRenderer _srCircle1;
	public SpriteRenderer _srCircle2;
	public SpriteRenderer _srImaginarycircle;

	int m_nStatus = 0;
	float m_fOffsetX = 0.0f;
	float m_fOffsetY = 0.0f;

	float m_fTimeCount = 0.0f;

	void Awake()
	{

	}

	// Use this for initialization
	void Start ()
	{
	//	this.gameObject.SetActive ( false );
	}
	
	// Update is called once per frame
	void Update () {
		Process ();
	}

	public void SetOffset( float fOffsetX, float fOffsetY )
	{
		m_fOffsetX = fOffsetX;
		m_fOffsetY = fOffsetY;
	}

	public void SetStatus( int nStatus )
	{
		m_nStatus = nStatus;
		m_fTimeCount = 0.0f;
	}

	float m_fDuration = 0f;
	public void Begin( float fDuration )
	{
		m_fDuration = fDuration;
		SetStatus (1);
		SetOffset ( 0.0f, -0.5f );
		_srCircle1.material.SetTextureOffset ("_Mask", new Vector2 (0.0f, -0.5f ));
		_srCircle1.material.SetColor ( "_Color", Color.red );
		_srCircle2.material.SetTextureOffset ("_Mask", new Vector2 (0.0f, 0.5f ));
		_srCircle2.material.SetColor ( "_Color", Color.red );
		this.gameObject.SetActive ( true );
		m_fTimeCount = 0.0f;
	}

	public void End()
	{
		SetStatus (3);
	}

	public void SetColor( Color color )
	{
//		_srCircle1.material.SetColor ( "_Color", color );
//		_srCircle1.color = color;
//		_srCircle2.color = color;
		_srImaginarycircle.color = color;
	}

	public int GetStatus()
	{
		return m_nStatus;
	}

	public float GetTimeCount()
	{
		return m_fTimeCount;
	}

	public void SetEnd ()
	{
		_srCircle1.material.SetTextureOffset ("_Mask", new Vector2 (0,0.5f));
		_srCircle2.material.SetTextureOffset ("_Mask", new Vector2 (0,-0.5f));
	}

	public void SetParams ( float fDuration, int nStatus, float fTimeCount )
	{
		m_fDuration = fDuration;
		if (nStatus == 1) {
			Begin (fDuration);
			m_fTimeCount = fTimeCount;
		} else if (nStatus == 2) {
			m_fOffsetY = 0.5f;
			SetStatus (2);
			_srCircle1.material.SetTextureOffset ("_Mask", new Vector2 (m_fOffsetX,m_fOffsetY));
			m_fTimeCount = fTimeCount;
		}
	}

	void Process()
	{
		if (m_nStatus == 1) {
			_srCircle1.material.SetTextureOffset ("_Mask", new Vector2 (m_fOffsetX, m_fOffsetY));
			//m_fOffsetY += Time.fixedDeltaTime * 1.2f;
			m_fTimeCount += Time.fixedDeltaTime;
			m_fOffsetY = -0.5f + m_fTimeCount / m_fDuration/*Main.s_Instance.m_fMainTriggerPreUnfoldTime*/ * 1.0f;
			if (m_fOffsetY >= 0.5f) {
				m_fOffsetY = 0.5f;
				SetStatus (2);
			}
		} else if (m_nStatus == 2) {
			_srCircle2.material.SetTextureOffset ("_Mask", new Vector2 (m_fOffsetX, m_fOffsetY));
			//m_fOffsetY -= Time.fixedDeltaTime * 1.2f;
			m_fTimeCount += Time.fixedDeltaTime;
			m_fOffsetY = 0.5f - m_fTimeCount / m_fDuration/*Main.s_Instance.m_fMainTriggerPreUnfoldTime*/ * 1.0f;
			if (m_fOffsetY <= -0.5f) {
				m_fOffsetY = -0.5f;
				End ();
			}
		}
	}

	public bool IsEnd()
	{
		return m_nStatus > 2;
	}













}
